use std::{
    collections::{HashMap, VecDeque},
    sync::atomic::AtomicU8,
};

use amethyst::{
    core::ecs::{
        storage::{DenseVecStorage, VecStorage},
        Component,
    },
    ecs::Entity,
};

#[derive(Debug, Default, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Position {
    pub x: u8,
    pub y: u8,
}

impl Component for Position {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug, Default)]
pub struct Cell {
    pub value: u8,
    pub additional_value: AtomicU8,
}

impl Component for Cell {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug)]
pub struct Neighbors {
    pub count: u8,
    pub cells: Vec<Position>,
}

impl Component for Neighbors {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug, Default)]
pub struct Possession {
    pub player_id: Option<u8>,
}

impl Component for Possession {
    type Storage = VecStorage<Self>;
}

#[derive(Debug, Default, Clone)]
pub struct Player {
    pub id: u8,
    pub color: [f32; 4],
    pub has_moved: bool,
}

impl PartialEq for Player {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Component for Player {
    type Storage = DenseVecStorage<Self>;
}

#[derive(Debug, Default)]
pub struct PlayerQueue {
    pub players: VecDeque<Player>,
}

#[derive(Debug, Default)]
pub struct Turn {
    pub player_id: u8,
    pub position: Position,
}

#[derive(Debug, Default)]
pub struct CellGrid<'a> {
    pub cells: HashMap<[u8; 2], &'a Cell>,
}

pub struct SideBoard {
    pub current_player: Entity,
}
