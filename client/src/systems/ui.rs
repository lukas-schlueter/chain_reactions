use amethyst::{
    core::shrev::EventChannel,
    ecs::{prelude::*, System},
    prelude::*,
    ui::{UiEvent, UiEventType, UiText},
};

use crate::components::{
    Cell, PlayerQueue, Position, Possession, SideBoard, Turn,
};

#[derive(Default)]
pub struct CurrentPlayer;

impl<'a> System<'a> for CurrentPlayer {
    type SystemData = (
        WriteStorage<'a, UiText>,
        ReadExpect<'a, SideBoard>,
        ReadExpect<'a, PlayerQueue>,
    );

    fn run(&mut self, (mut ui_text, sideboard, players): Self::SystemData) {
        let current_player = players
            .players
            .front()
            .expect("Could not read current player");
        let current_player_label = ui_text
            .get_mut(sideboard.current_player)
            .expect("Failed to get label for 'current_player'");
        current_player_label.color = current_player.color;
    }
}

#[derive(Default)]
pub struct Renderer;

impl<'a> System<'a> for Renderer {
    type SystemData = (
        ReadStorage<'a, Cell>,
        WriteStorage<'a, UiText>,
        ReadStorage<'a, Possession>,
        ReadExpect<'a, PlayerQueue>,
    );

    fn run(
        &mut self,
        (cells, mut texts, possessions, players): Self::SystemData,
    ) {
        for (cell, text, possession) in
            (&cells, &mut texts, &possessions).join()
        {
            text.text = cell.value.to_string();
            if let Some(player_id) = possession.player_id {
                text.color = players
                    .players
                    .iter()
                    .find(|p| p.id == player_id)
                    .map(|p| p.color)
                    .unwrap_or([1., 1., 1., 5.]);
            } else {
                text.color = [1., 1., 1., 1.];
            }
        }
    }
}

#[derive(SystemDesc)]
#[system_desc(name(InputHandlerDesc))]
pub struct InputHandler {
    #[system_desc(event_channel_reader)]
    reader_id: ReaderId<UiEvent>,
}

impl InputHandler {
    fn new(reader_id: ReaderId<UiEvent>) -> Self {
        Self { reader_id }
    }
}

impl<'a> System<'a> for InputHandler {
    type SystemData = (
        Read<'a, EventChannel<UiEvent>>,
        Read<'a, PlayerQueue>,
        ReadStorage<'a, Position>,
        Write<'a, EventChannel<Turn>>,
    );

    fn run(
        &mut self,
        (ui_events, players, positions, mut turn_channel): Self::SystemData,
    ) {
        for ev in ui_events.read(&mut self.reader_id) {
            if let UiEventType::Click = ev.event_type {
                if let Some(position) = positions.get(ev.target) {
                    let current_player =
                        players.players.front().expect("No player found!");

                    turn_channel.single_write(Turn {
                        player_id: current_player.id,
                        position: *position,
                    })
                }
            }
        }
    }
}
