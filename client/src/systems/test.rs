use std::{collections::HashMap, sync::atomic::Ordering};

use amethyst::{
    core::ecs::{prelude::*, System},
    ui::UiText,
};
use log::*;

use crate::{components::*, states::SplitterCount};

#[derive(Default)]
pub struct Adder {}

impl<'a> System<'a> for Adder {
    type SystemData = (
        WriteStorage<'a, Cell>,
        WriteStorage<'a, UiText>,
        ReadStorage<'a, Possession>,
        ReadExpect<'a, PlayerQueue>,
    );

    fn run(
        &mut self,
        (mut cells, _texts, _possessions, _players): Self::SystemData,
    ) {
        debug!("Adding!");
        for cell in (&mut cells).join() {
            debug!("Value before add: {:?}", cell);

            // TODO: Is this good?
            if cell.value > 240 {
                cell.value -= 240;
            }

            cell.value += cell.additional_value.swap(0, Ordering::Relaxed);
        }
    }
}

#[derive(Default)]
pub struct Splitter {}

impl<'a> System<'a> for Splitter {
    type SystemData = (
        WriteStorage<'a, Cell>,
        ReadStorage<'a, Neighbors>,
        ReadStorage<'a, Position>,
        WriteExpect<'a, SplitterCount>,
        WriteStorage<'a, Possession>,
    );

    fn run(
        &mut self,
        (
            mut cells,
            neighbors,
            positions,
            mut splitter_count,
            mut possessions,
        ): Self::SystemData,
    ) {
        debug!("Splitting");

        let mut new_values = HashMap::<Position, (u8, Option<u8>)>::new();

        for (cell, cell_neighbors, possession) in
            (&mut cells, &neighbors, &mut possessions).join()
        {
            debug!("Value before split: {:?}", cell);

            if cell.value >= cell_neighbors.count {
                for &pos in &cell_neighbors.cells {
                    //                let value =
                    //                    new_values.get_mut(&pos).or(Some(&mut
                    // 0u8)).unwrap();                *value += 1;
                    let (value, _) =
                        new_values.get(&pos).unwrap_or(&(0u8, None));

                    let new_value = value + 1;
                    new_values.insert(pos, (new_value, possession.player_id));
                    trace!("{:?}", new_values);
                }
                cell.value -= cell_neighbors.count;

                if cell.value == 0 {
                    possession.player_id = None;
                    info!(
                        "Cell lost possession: {:?} {:?} {:?}",
                        cell, cell_neighbors, possession
                    );
                }
            }
        }

        for (cell, pos, possession) in
            (&mut cells, &positions, &mut possessions).join()
        {
            if let Some((value, owner)) = new_values.get(pos) {
                debug!("Adding {} to {:?} at {:?}", value, cell, pos);
                cell.additional_value.fetch_add(*value, Ordering::Relaxed);
                possession.player_id = *owner;
            }
        }

        //        for (cell, text) in (&mut cells, &mut texts).join() {
        //            text.text = cell.value.to_string();
        //        }

        println!("Next Splitter count: {:?}", new_values.len());
        *splitter_count = SplitterCount(new_values.len());
        println!("Updated Splitter count to:  {:?}", *splitter_count);
    }
}
