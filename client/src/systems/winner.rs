use std::collections::HashMap;

use amethyst::{ecs::prelude::*, shrev::EventChannel};
use log::*;

use crate::{
    components::{PlayerQueue, Possession},
    states::AppEvent,
};

#[derive(Default)]
pub struct WinnerSystem;

impl<'a> System<'a> for WinnerSystem {
    type SystemData = (
        Write<'a, EventChannel<AppEvent>>,
        Write<'a, PlayerQueue>,
        ReadStorage<'a, Possession>,
    );

    fn run(
        &mut self,
        (mut event_channel, mut player_queue, possessions): Self::SystemData,
    ) {
        info!("Checking for winner!");

        let mut cells_for_player_id = HashMap::new();

        for possession in (&possessions).join() {
            let player_id = possession.player_id;

            if let Some(id) = player_id {
                *cells_for_player_id.entry(id).or_insert(0u8) += 1;
            }
        }

        info!("Counted cells: {:?}", cells_for_player_id);

        let mut losers = vec![];

        for player in &player_queue.players {
            let cells = cells_for_player_id.get(&player.id).unwrap_or(&0u8);

            info!("Player {:?} has {} cells", player, cells);
            if *cells == 0u8 && player.has_moved {
                // Player lost
                losers.push(player.id);
                info!("Player {:?} lost!", player);
            }
        }

        player_queue
            .players
            .retain(|player| !losers.contains(&player.id));

        if player_queue.players.len() == 1 {
            info!("Game over, winner: {:?}", player_queue.players.front());
            event_channel.single_write(AppEvent::Winner(
                (*player_queue.players.front().unwrap()).clone(),
            ));
        } else {
        }
    }
}
