use std::sync::atomic::Ordering;

use amethyst::{core::shrev::EventChannel, ecs::prelude::*, prelude::*};
use log::*;

use crate::{
    components::{Cell, PlayerQueue, Position, Possession, Turn},
    config::GameConfig,
    states::{CurrentState, SplitterCount},
};

#[derive(SystemDesc)]
#[system_desc(name(TurnHandlerDesc))]
pub struct TurnHandler {
    #[system_desc(event_channel_reader)]
    reader_id: ReaderId<Turn>,
}

impl TurnHandler {
    fn new(reader_id: ReaderId<Turn>) -> Self {
        Self { reader_id }
    }
}

impl<'a> System<'a> for TurnHandler {
    type SystemData = (
        Read<'a, EventChannel<Turn>>,
        Write<'a, PlayerQueue>,
        ReadStorage<'a, Position>,
        WriteStorage<'a, Possession>,
        WriteStorage<'a, Cell>,
        Write<'a, CurrentState>,
    );

    fn run(
        &mut self,
        (
            channel,
            mut players,
            positions,
            mut possessions,
            mut cells,
            mut current_state,
        ): Self::SystemData,
    ) {
        let current_player_id =
            players.players.front().expect("No payer found!").id;

        for turn in channel.read(&mut self.reader_id) {
            info!("Received a turn: {:?}", turn);

            if turn.player_id != current_player_id {
                warn!("Invalid move!");
                continue;
            }

            if *current_state != CurrentState::WaitingForInput {
                warn!("Can't move at the moment!");
                continue;
            }

            let (mut possession, mut cell) = (None, None);

            for (position, poss, c) in
                (&positions, &mut possessions, &mut cells).join()
            {
                if *position == turn.position {
                    possession = Some(poss);
                    cell = Some(c);
                    break;
                }
            }

            let possession = possession.unwrap_or_else(|| {
                panic!("Could not find possession for turn {:?}", turn)
            });
            let cell = cell.unwrap_or_else(|| {
                panic!("Could not find cell for turn {:?}", turn)
            });

            if possession.player_id != Some(turn.player_id)
                && possession.player_id != None
            {
                warn!("Invalid move!");
                continue;
            }

            cell.additional_value.fetch_add(1, Ordering::Relaxed);

            info!(
                "Cell has new owner: {} (old: {:?})",
                turn.player_id, possession.player_id
            );

            possession.player_id.replace(turn.player_id);

            let mut current_player = players.players.pop_front().unwrap(); // Should never fail
            current_player.has_moved = true;
            players.players.push_back(current_player);

            *current_state = CurrentState::Add;
            return;
        }
    }
}

#[derive(Default, Debug)]
pub struct StateHandlerDesc {}

impl<'a, 'b> SystemDesc<'a, 'b, StateHandler> for StateHandlerDesc {
    fn build(self, world: &mut World) -> StateHandler {
        <StateHandler as System<'_>>::SystemData::setup(world);

        let split_delay = {
            let config = world.read_resource::<GameConfig>();
            config.split_delay
        };

        StateHandler::new(split_delay)
    }
}

//#[derive(SystemDesc)]
//#[system_desc(name(StateHandlerDesc))]
pub struct StateHandler {
    //    #[system_desc(event_channel_reader)]
    //    reader_id: ReaderId<Turn>,
    counter: usize,
}

impl StateHandler {
    fn new(counter: usize) -> Self {
        Self { counter }
    }
}

impl<'a> System<'a> for StateHandler {
    type SystemData = (
        Read<'a, GameConfig>,
        Write<'a, CurrentState>,
        Read<'a, SplitterCount>,
    );

    fn run(
        &mut self,
        (config, mut current_state, splitter_count): Self::SystemData,
    ) {
        match &*current_state {
            CurrentState::Invalid => {}
            CurrentState::WaitingForInput => {}
            CurrentState::Add => {
                *current_state =
                    CurrentState::WaitingFor(Box::new(CurrentState::Split));
            }
            CurrentState::Split => {
                println!("Splitter count: {:?}", *splitter_count);
                if splitter_count.0 > 0 {
                    *current_state =
                        CurrentState::WaitingFor(Box::new(CurrentState::Add));
                } else {
                    *current_state = CurrentState::WaitingFor(Box::new(
                        CurrentState::WaitingForInput,
                    ));
                }
            }
            CurrentState::WaitingFor(x) => {
                self.counter -= 1;

                if self.counter == 0 {
                    self.counter = config.split_delay;
                    *current_state = *x.clone();
                }
            }
        }
    }
}
