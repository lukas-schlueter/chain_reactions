use amethyst::{
    ecs::{prelude::*, Entity},
    prelude::*,
    ui::{UiEventType, UiTransform},
    GameData, StateData,
};
use log::*;

use crate::{
    states::{game::GameState, loading::UiPrefabRegistry, GameEvent},
    util::delete_hierarchy,
};

#[derive(Default)]
pub struct MainMenu {
    initialized: bool,
    ui_parent: Option<Entity>,
}

impl<'a, 'b> State<GameData<'a, 'b>, GameEvent> for MainMenu {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        info!("Welcome to MainMenu!");
        let world = data.world;

        info!("Loading UI");
        let menu_prefab = world
            .read_resource::<UiPrefabRegistry>()
            .find(world, "menu");
        info!("Done");

        if let Some(menu_prefab) = menu_prefab {
            info!("Creating entity");
            self.ui_parent =
                Some(world.create_entity().with(menu_prefab).build());
            info!("Updating world");
            data.data.update(world);
        } else {
            error!("Could not find menu prefab!");
        }
    }

    fn on_stop(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        if let Some(root) = self.ui_parent {
            delete_hierarchy(root, data.world)
                .expect("failed to delete menu items");
        }
        self.ui_parent = None;
        self.initialized = false; // Or is it?
    }

    fn handle_event(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
        event: GameEvent,
    ) -> Trans<GameData<'a, 'b>, GameEvent> {
        //        println!("{:?}", event);
        if let GameEvent::Ui(e) = event {
            //                                println!("UiEvent: {:?}", e);
            if let UiEventType::Click = e.event_type {
                println!("Clicked on {:?} ({:?})", e.target, e.target.id());
                let store = data.world.read_storage::<UiTransform>();
                if let Some(transform) = store.get(e.target) {
                    return match transform.id.as_str() {
                        "btn_quit" => Trans::Quit,
                        _ => Trans::Switch(Box::new(GameState::default())),
                    };
                }
            }
        }
        Trans::None
    }

    fn update(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
    ) -> Trans<GameData<'a, 'b>, GameEvent> {
        data.data.update(&data.world);
        Trans::None
    }
}
