use amethyst::{
    assets::Loader,
    core::{
        ecs::{Read, SystemData},
        shrev::{EventChannel, ReaderId},
        transform::components::Parent,
        EventReader,
    },
    ecs::{Dispatcher, DispatcherBuilder, Entity, WorldExt},
    prelude::{Builder, SystemDesc, SystemExt, World},
    ui::{
        Anchor, Interactable, TtfFormat, UiEvent, UiFinder, UiText, UiTransform,
    },
    winit::Event,
    GameData, State, StateData, Trans,
};
use log::*;

use crate::{
    components::{
        Cell, Neighbors, Player, PlayerQueue, Position, Possession, SideBoard,
        Turn,
    },
    config::GameConfig,
    states::{loading::UiPrefabRegistry, WinnerState},
    systems::{
        game::{StateHandlerDesc, TurnHandlerDesc},
        test::{Adder, Splitter},
        ui::{InputHandlerDesc, Renderer},
        winner::WinnerSystem,
        CurrentPlayer,
    },
    util::delete_hierarchy,
};
use std::ops::Deref;

#[derive(Debug, Clone, Eq, PartialEq)]
pub enum CurrentState {
    Invalid,
    WaitingForInput,
    Add,
    Split,
    WaitingFor(Box<CurrentState>),
}

impl Default for CurrentState {
    fn default() -> Self {
        Self::Invalid
    }
}

#[derive(Default, Debug)]
pub struct SplitterCount(pub usize);

#[derive(Debug, Clone)]
pub enum AppEvent {
    Winner(Player),
}

#[derive(Debug, EventReader, Clone)]
#[reader(GameEventReader)]
pub enum GameEvent {
    Ui(UiEvent),
    App(AppEvent),
    Window(Event),
}

pub struct GameState<'a, 'b> {
    ui_root: Option<Entity>,
    dispatcher: Option<Dispatcher<'a, 'b>>,
    counter: usize,
}

impl<'a, 'b> Default for GameState<'a, 'b> {
    fn default() -> Self {
        Self {
            ui_root: None,
            dispatcher: None,
            counter: 1,
        }
    }
}

impl<'a, 'b> State<GameData<'a, 'b>, GameEvent> for GameState<'_, '_> {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        println!("Registering components!");
        let world = data.world;
        world.register::<Cell>();
        world.register::<Position>();
        world.register::<Neighbors>();
        world.register::<Possession>();
        world.register::<Player>();
        world.insert(EventChannel::<GameEvent>::new());
        world.insert(EventChannel::<Turn>::new());

        let mut queue = PlayerQueue::default();
        queue.players.push_back(Player {
            id: 0,
            color: [1., 0., 0., 1.],
            has_moved: false,
        });
        queue.players.push_back(Player {
            id: 1,
            color: [0., 0.8, 0., 1.],
            has_moved: false,
        });
        world.insert(queue);
        println!("Added Players!");

        println!("Building dispatchers!");
        self.dispatcher = Some(build_dispatcher(world));
        self.dispatcher.as_mut().unwrap().setup(world);

        let game_prefab = world
            .read_resource::<UiPrefabRegistry>()
            .find(world, "game");

        println!("Loading UI!");
        if let Some(menu_prefab) = game_prefab {
            self.ui_root =
                Some(world.create_entity().with(menu_prefab).build());
            data.data.update(world);
            let current_player_label = world.exec(|finder: UiFinder<'_>| {
                finder
                    .find("current_player")
                    .expect("failed to find label for 'current_player")
            });
            world.insert(SideBoard {
                current_player: current_player_label,
            });
        } else {
            error!("Could not find game prefab!");
        }

        initialize_cell_grid(world);
        data.data.update(world);

        println!("Hello game!");

        let mut current_state = world.write_resource::<CurrentState>();
        *current_state = CurrentState::Add;
    }

    fn on_stop(&mut self, data: StateData<'_, GameData<'a, 'b>>) {
        data.world.remove::<SideBoard>();
        if let Some(root) = self.ui_root {
            delete_hierarchy(root, data.world)
                .expect("failed to delete game items");
        }
        data.world.remove::<EventChannel<Turn>>();
        data.world.remove::<EventChannel<GameEvent>>();
        self.ui_root = None;
        self.counter = 1;
        self.dispatcher = None;
    }

    fn handle_event(
        &mut self,
        _data: StateData<'_, GameData<'_, '_>>,
        event: GameEvent,
    ) -> Trans<GameData<'a, 'b>, GameEvent> {
        if let GameEvent::App(event) = event {
            match event {
                AppEvent::Winner(player) => {
                    info!("Game over, pushing state!");
                    return Trans::Push(Box::new(WinnerState::new(player)));
                }
            }
        }
        Trans::None
    }

    fn update(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
    ) -> Trans<GameData<'a, 'b>, GameEvent> {
        data.data.update(&data.world);
        let dispatcher = self
            .dispatcher
            .as_mut()
            .expect("Failed to unwrap dispatcher!");
        dispatcher.dispatch(&data.world);
        Trans::None
    }

    /* fn update(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
    ) -> Trans<GameData<'a, 'b>, GameEvent> {
        data.data.update(&data.world);

        let split_delay = {
            let config = data.world.read_resource::<GameConfig>();
            config.split_delay
        };

        let current_state =
            (*data.world.read_resource::<CurrentState>()).clone();

        trace!("Current State: {:?}", current_state);

        self.counter -= 1;
        if self.counter == 0 {
            self.counter = split_delay;
            let dispatcher = self
                .dispatcher
                .as_mut()
                .expect("Failed to unwrap dispatcher!");
            dispatcher.dispatch(&data.world);

            match current_state {
                CurrentState::Invalid => {}
                CurrentState::WaitingForInput => {}
                CurrentState::Add => {
                    let mut current_state =
                        data.world.write_resource::<CurrentState>();
                    *current_state = CurrentState::Split;
                }
                CurrentState::Split => {
                    let splitter_count =
                        data.world.read_resource::<SplitterCount>();
                    let mut current_state =
                        data.world.write_resource::<CurrentState>();

                    if splitter_count.0 > 0 {
                        *current_state = CurrentState::Add;
                    } else {
                        *current_state = CurrentState::WaitingForInput;
                    }
                }
            };
        }
        Trans::None
    }*/
}

fn build_dispatcher<'a, 'b>(world: &mut World) -> Dispatcher<'a, 'b> {
    DispatcherBuilder::new()
        .with(CurrentPlayer, "current_player", &[])
        .with(
            InputHandlerDesc::default().build(world),
            "input_handler",
            &["current_player"],
        )
        .with(
            TurnHandlerDesc::default().build(world),
            "turn_handler",
            &["input_handler"],
        )
        .with(
            StateHandlerDesc::default().build(world),
            "state_handler",
            &[],
        )
        .with_barrier()
        .with(
            Adder::default().pausable(CurrentState::Add),
            "add_system",
            &[],
        )
        .with(
            Splitter::default().pausable(CurrentState::Split),
            "split_system",
            &[],
        )
        .with_barrier()
        .with(
            Renderer::default().pausable(CurrentState::Add),
            "render_system",
            &[],
        )
        .with(
            WinnerSystem::default().pausable(CurrentState::Add),
            "winner_system",
            &[],
        )
        .with_barrier()
        .build()
}

fn initialize_cell_grid(world: &mut World) {
    let parent = world
        .exec(|finder: UiFinder<'_>| finder.find("cell_container"))
        .expect("Failed to fetch cell container");

    let font = world.read_resource::<Loader>().load(
        "fonts/square.ttf",
        TtfFormat,
        (),
        &world.read_resource(),
    );

    let config = world.read_resource::<GameConfig>().deref().clone();
    let size_x = config.size;
    let size_y = config.size;

    let do_portal_center = config.portal_center;

    for x in 0..size_x {
        for y in 0..size_y {
            let neighbors =
                get_neighbors(x, y, size_x, size_y, do_portal_center);

            let pos = Position { x, y };
            let cell = Cell::default();

            let ui_transform = UiTransform::new(
                format!("cell_{}_{}", pos.x, pos.y),
                Anchor::TopLeft,
                Anchor::TopLeft,
                5. + 30. * (pos.x as f32) as f32,
                -5. - 30. * (pos.y as f32) as f32,
                0.1,
                25.,
                25.,
            );
            println!(
                "x: {} y: {}",
                5. + 30. * (pos.x as f32) as f32,
                -5. - 30. * (pos.y as f32) as f32
            );

            let ui_label = UiText::new(
                font.clone(),
                cell.value.to_string(),
                [1., 1., 1., 1.],
                25.,
            );

            world
                .create_entity()
                .with(cell)
                .with(pos)
                .with(Neighbors {
                    count: neighbors.len() as u8,
                    cells: neighbors,
                })
                .with(ui_transform)
                .with(ui_label)
                .with(Interactable)
                .with(Possession::default())
                .with(Parent { entity: parent })
                .build();
        }
    }
}

fn get_neighbors(
    x: u8,
    y: u8,
    size_x: u8,
    size_y: u8,
    do_portal_center: bool,
) -> Vec<Position> {
    if do_portal_center && x == size_x / 2 && y == size_y / 2 {
        return vec![
            Position { x: 0, y: 0 },
            Position {
                x: 0,
                y: size_y - 1,
            },
            Position {
                x: size_x - 1,
                y: 0,
            },
            Position {
                x: size_x - 1,
                y: size_y - 1,
            },
        ];
    }

    let mut neighbors = vec![];

    if x > 0 {
        let left = Position { x: x - 1, y };
        neighbors.push(left);
    }
    if x < size_x - 1 {
        let right = Position { x: x + 1, y };
        neighbors.push(right);
    }

    if y > 0 {
        let up = Position { x, y: y - 1 };
        neighbors.push(up);
    }
    if y < size_y - 1 {
        let down = Position { x, y: y + 1 };
        neighbors.push(down);
    }

    neighbors
}
