use amethyst::{
    ecs::Entity,
    input::is_close_requested,
    prelude::{Builder, WorldExt, *},
    shrev::EventChannel,
    ui::{UiEvent, UiEventType, UiFinder, UiText},
    GameData, State, StateData, TransEvent,
};
use log::*;

use crate::{
    components::Player,
    states::{loading::UiPrefabRegistry, GameEvent, MainMenu},
    util::delete_hierarchy,
};

pub struct WinnerState {
    winner: Player,
    menu_button: Option<Entity>,
    root: Option<Entity>,
}

impl WinnerState {
    pub fn new(winner: Player) -> Self {
        Self {
            winner,
            menu_button: None,
            root: None,
        }
    }
}

impl<'a, 'b> State<GameData<'a, 'b>, GameEvent> for WinnerState {
    fn on_start(&mut self, data: StateData<'_, GameData<'_, '_>>) {
        println!("Congratulations!");
        let world = data.world;

        let winner_prefab = world
            .read_resource::<UiPrefabRegistry>()
            .find(world, "winner");

        if let Some(winner_prefab) = winner_prefab {
            self.root = Some(world.create_entity().with(winner_prefab).build());
            data.data.update(world);

            let winner_label = world
                .exec(|finder: UiFinder<'_>| finder.find("winner_text"))
                .expect("Failed to fetch winner_text");

            let mut store = world.write_storage::<UiText>();
            let mut text = store
                .get_mut(winner_label)
                .expect("winner_text has no text!");

            text.text = format!("Player {} wins!", self.winner.id);
        } else {
            error!("Could not find winner prefab!");
        }
    }

    fn on_stop(&mut self, data: StateData<'_, GameData<'a, 'b>>) {
        if let Some(root) = self.root {
            delete_hierarchy(root, data.world)
                .expect("Failed to remove winning screen");
        }
        self.root = None;
        self.menu_button = None;
    }

    fn handle_event(
        &mut self,
        data: StateData<'_, GameData<'a, 'b>>,
        event: GameEvent,
    ) -> Trans<GameData<'a, 'b>, GameEvent> {
        match event {
            GameEvent::Window(event) => {
                if is_close_requested(&event) {
                    info!("[Trans::Quit] Quitting Application!");
                    Trans::Quit
                } else {
                    Trans::None
                }
            }
            GameEvent::Ui(UiEvent {
                event_type: UiEventType::Click,
                target,
            }) => {
                if Some(target) == self.menu_button {
                    info!("Clicked on menu_button!");
                    let mut trans_event_channel =
                        data.world.write_resource::<EventChannel<
                            TransEvent<GameData<'_, '_>, GameEvent>,
                        >>();

                    trans_event_channel.single_write(Box::new(|| Trans::Pop));
                    trans_event_channel.single_write(Box::new(|| {
                        Trans::Switch(Box::new(MainMenu::default()))
                    }));
                    Trans::None
                } else {
                    info!("Clicked on {:?}", target);
                    Trans::None
                }
            }
            _ => Trans::None,
        }
    }

    fn update(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
    ) -> Trans<GameData<'a, 'b>, GameEvent> {
        if self.menu_button.is_none() {
            data.world.exec(|finder: UiFinder<'_>| {
                self.menu_button = finder.find("menu_button");
            })
        }
        data.data.update(&data.world);
        Trans::None
    }
}
