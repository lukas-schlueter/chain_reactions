use crate::states::GameEvent;
use amethyst::{
    assets::{AssetStorage, Completion, Handle, ProgressCounter},
    ecs::{Entity, World},
    input::{is_close_requested, is_key_down},
    prelude::*,
    ui::{UiCreator, UiLoader, UiPrefab},
    winit::VirtualKeyCode,
};
use log::info;

use super::MainMenu;
use crate::util::delete_hierarchy;

// UiPrefabRegistry from https://github.com/amethyst/evoli/blob/c06a23679b662c8aa9611ad87a283a7bc3b8c451/src/resources/prefabs.rs
#[derive(Default)]
pub struct UiPrefabRegistry {
    pub prefabs: Vec<Handle<UiPrefab>>,
}

impl UiPrefabRegistry {
    pub fn register(&mut self, prefab: Handle<UiPrefab>) {
        self.prefabs.push(prefab)
    }

    pub fn find(&self, world: &World, name: &str) -> Option<Handle<UiPrefab>> {
        let storage = world.read_resource::<AssetStorage<UiPrefab>>();
        self.prefabs.iter().find_map(|handle| {
            if storage
                .get(handle)?
                .entities()
                .next()?
                .data()?
                .0 // transform is 0th element of UiPrefab tuple
                .as_ref()?
                .id
                == name
            {
                Some(handle.clone())
            } else {
                None
            }
        })
    }
}

#[derive(Default)]
pub struct Loading {
    progress: Option<ProgressCounter>,
    load_ui: Option<Entity>,
    /* menu_ui: Option<Handle<UiPrefab>>, */
}

impl<'a, 'b> State<GameData<'a, 'b>, GameEvent> for Loading {
    fn on_start(&mut self, mut data: StateData<'_, GameData<'_, '_>>) {
        self.load_ui = Some(data.world.exec(|mut creator: UiCreator<'_>| {
            creator.create("ui/loading.ron", &mut ProgressCounter::default())
        }));
        /*
        self.menu_ui = Some(data.world.exec(|loader: UiLoader<'_>| {
            loader.load("ui/menu.ron", &mut self.progress)
        }));*/
        self.progress = Some(load_prefabs(&mut data.world));
        info!("Started loading assets");
    }

    fn on_stop(&mut self, data: StateData<'_, GameData<'a, 'b>>) {
        if let Some(handler) = self.load_ui {
            delete_hierarchy(handler, data.world)
                .expect("Failed to remove LoadingScreen");
        }
        self.load_ui = None;
        self.progress = None;
    }

    fn handle_event(
        &mut self,
        _data: StateData<'_, GameData<'_, '_>>,
        event: GameEvent,
    ) -> Trans<GameData<'a, 'b>, GameEvent> {
        if let GameEvent::Window(event) = event {
            if is_close_requested(&event)
                || is_key_down(&event, VirtualKeyCode::Escape)
            {
                return Trans::Quit;
            }
        }
        Trans::None
    }

    fn update(
        &mut self,
        data: StateData<'_, GameData<'_, '_>>,
    ) -> Trans<GameData<'a, 'b>, GameEvent> {
        data.data.update(&data.world);

        if let Some(counter) = &self.progress {
            info!(
                "Loading Assets:\tTotal: {} Finished: {} Loading: {}",
                counter.num_assets(),
                counter.num_finished(),
                counter.num_loading()
            );

            return match counter.complete() {
                Completion::Failed => {
                    eprintln!("Failed loading assets");
                    Trans::Quit
                }
                Completion::Complete => {
                    info!("Assets loaded, swapping state");
                    //                    if let Some(entity) = self.load_ui {
                    //                        let _ =
                    // data.world.delete_entity(entity);
                    //                    }
                    // Letting on_stop run instead

                    Trans::Switch(Box::new(MainMenu::default()))
                }
                Completion::Loading => Trans::None,
            };
        }
        Trans::None
    }
}

fn load_prefabs(world: &mut World) -> ProgressCounter {
    let mut progress_counter = ProgressCounter::default();

    let mut ui_prefab_registry = UiPrefabRegistry::default();

    ui_prefab_registry.register(world.exec(|loader: UiLoader<'_>| {
        loader.load("ui/menu.ron", &mut progress_counter)
    }));

    ui_prefab_registry.register(world.exec(|loader: UiLoader<'_>| {
        loader.load("ui/game.ron", &mut progress_counter)
    }));

    ui_prefab_registry.register(world.exec(|loader: UiLoader<'_>| {
        loader.load("ui/winner.ron", &mut progress_counter)
    }));

    world.insert(ui_prefab_registry);
    progress_counter
}
