#![warn(
    missing_debug_implementations,
    missing_docs,
    rust_2018_idioms,
    rust_2018_compatibility
)]
#[macro_use]
extern crate amethyst;

use std::time::Duration;

use amethyst::{
    assets::{HotReloadBundle, HotReloadStrategy},
    core::{frame_limiter::FrameRateLimitStrategy, transform::TransformBundle},
    input::{InputBundle, StringBindings},
    prelude::*,
    renderer::{
        plugins::{RenderFlat2D, RenderToWindow},
        types::DefaultBackend,
        RenderingBundle,
    },
    ui::{RenderUi, UiBundle},
    utils::{application_root_dir, fps_counter::FpsCounterBundle},
    LoggerConfig,
};

use crate::{
    components::CellGrid,
    config::GameConfig,
    states::{GameEvent, GameEventReader},
};

mod components;
mod config;
mod state;
mod states;
mod systems;
mod util;

fn main() -> amethyst::Result<()> {
    amethyst::start_logger(LoggerConfig::default());

    let app_root = application_root_dir()?;

    let resources = app_root.join("resources");
    let config = app_root.join("config");

    let display_config = config.join("display.ron");

    let game_config = GameConfig::load(config.join("game.ron"));

    let game_data = GameDataBuilder::default()
        .with_bundle(TransformBundle::new())?
        .with_bundle(HotReloadBundle::new(HotReloadStrategy::every(1)))?
        .with_bundle(
            RenderingBundle::<DefaultBackend>::new()
                .with_plugin(
                    RenderToWindow::from_config_path(display_config)
                        .with_clear([0.34, 0.36, 0.52, 1.0]),
                )
                .with_plugin(RenderFlat2D::default())
                .with_plugin(RenderUi::default()),
        )?
        .with_bundle(UiBundle::<StringBindings>::new())?
        .with_bundle(InputBundle::<StringBindings>::new())?
        .with_bundle(FpsCounterBundle)?;

    let mut game = CoreApplication::<_, GameEvent, GameEventReader>::build(
        //		Application::build(
        resources,
        states::Loading::default(),
    )?
    .with_frame_limit(
        // FrameRateLimitStrategy::Sleep,
        FrameRateLimitStrategy::SleepAndYield(Duration::from_millis(2)),
        75,
    )
    .with_resource(game_config)
    .with_resource(CellGrid::default())
    .with_resource(states::CurrentState::Invalid)
    .with_resource(states::SplitterCount(0))
    .build(game_data)?;
    game.run();

    Ok(())
}
