use serde::{Deserialize, Serialize};

#[derive(Default, Debug, Deserialize, Serialize, Clone)]
pub struct GameConfig {
    pub split_delay: usize,
    pub portal_center: bool,
    pub size: u8,
}
